<?php

namespace Imd\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $bookings = $qb->select('b')
                       ->from('ImdAppBundle:Booking', 'b')
                       ->where('b.feedback IS NOT NULL')
                       ->getQuery()
                       ->getResult();

        $qb = $em->createQueryBuilder();
        $imders =   $qb->select('u')
                       ->from('ImdAppBundle:User', 'u')
                       ->where('u.roles LIKE :role')
                       ->andwhere('u.enabled = :enabled')
                       ->setParameter('role', '%ROLE_IMD%')
                       ->setParameter('enabled', 1)
                       //->setMaxResults(3)
                       ->getQuery()
                       ->getResult();

        shuffle($imders);
        shuffle($bookings);

        $ppImders = $this->ppLimit($imders,4);
        $ppBookings = $this->ppLimit($bookings,2);

        $data = [
            'bookings' => $ppBookings,
            'imders' => $ppImders
        ];

        return $this->render('ImdAppBundle:Default:index.html.twig', $data);
    }

    public function ppLimit(array $items, $quantity){
      
      if ($items !== []) {
        $result = [];
        // loop four times
        for ($i=0; $i <= $quantity-1; $i++) {
          // check if null
          if (isset($items[$i])) {
            array_push($result, $items[$i]);
          }
        }
      }else{
        return [];
      }

      return $result;
    }
}
