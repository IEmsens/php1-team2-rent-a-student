<?php

namespace Imd\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DebugController extends Controller
{
    public function indexAction()
    {
        $user = $this->getUser();
        return $this->render('ImdAppBundle:Debug:index.html.twig', array('user' => $user));
    }

    public function makeAdminAction(Request $request)
    {
        if($request->isMethod('POST')) {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserBy(array('id' => $this->get('request')->request->get('userId')));
            $user->switchAdmin();  
            $userManager->updateUser($user);
        }

        return $this->redirectToRoute('imd_debug');
    }
}