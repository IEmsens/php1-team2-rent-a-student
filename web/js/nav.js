$(document).ready(function(){
    console.log('loaded nav');

    // Caches a jQuery object containing the header element
    var navbar = $('#navigation');
    var front = $('#front-page');
    var expand = $('button[data-toggle="collapse"]');

    console.log();

    // Function that adds the opaque class to the given
    // jQuery element
    function opaque(nav){
        var scroll = $(window).scrollTop();

        if (scroll > 0) {
            nav.addClass('opaque');
        } else if(expand.attr('aria-expanded') != "true") {
            if (nav.hasClass('opaque')){
                nav.removeClass('opaque');
            }
        }
    }

    // Function that adds transitional effect to the
    // given element this prevents navbar fadein on page
    // refresh if page is already scrolled
    function addTransition(nav){
        setTimeout(function() {
            nav.css('transition','background-color ease-in-out 0.2s, box-shadow ease-in-out 0.2s')
        }, 200);
    }

    opaque(navbar);
    addTransition(navbar);

    // Checks whether or not the current page is the front
    // page by checking if the div#front-page is present
    // because front is the only one which needs clear menu
    if (front.length){
        $(window).scroll(function() {           
            opaque(navbar);
        });
    }else{
        if (!navbar.hasClass('opaque')){
            navbar.addClass('opaque');
        }
    }

    expand.on('click',function(){
        if (!navbar.hasClass('opaque')){
            navbar.addClass('opaque');
        }else{
            setTimeout(function() {
                opaque(navbar);    
            }, 100);
        }
    });

    var footer = $("footer");
    if ($("body").height() > $(window).height()) {
        footer.css('position','relative');
    }else{
        footer.css('position','absolute');
    }

    $(window).on('resize', function(){
        if ($("body").height() > $(window).height()) {
            footer.css('position','relative');
        }else{
            footer.css('position','absolute');
        }
    });

});